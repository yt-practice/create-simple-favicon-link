import React, { useEffect } from 'react'
import type { AppProps } from 'next/app'
import Head from 'next/head'

const App = ({ Component, pageProps }: AppProps) => {
	useEffect(() => {
		// Remove the server-side injected CSS.
		const jssStyles = document.querySelector('#jss-server-side')
		jssStyles?.parentElement?.removeChild(jssStyles)
	}, [])

	return (
		<>
			<Head>
				<meta charSet="utf-8" />
				<title>create simple favicon link</title>
				<meta
					name="viewport"
					content="minimum-scale=1,initial-scale=1,width=device-width"
				/>
				<meta name="description" content="for developers." />
				<link
					rel="icon"
					type="image/svg+xml"
					href={`data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg"><text y="32" font-size="32">🚧</text></svg>`}
				/>
			</Head>
			<Component {...pageProps} />
		</>
	)
}

export default App

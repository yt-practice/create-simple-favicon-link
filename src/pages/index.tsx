import React, { useState } from 'react'

import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'

import { useRefForClipboard } from '~/libs/useRefForClipboard'
import { asLink } from '~/libs/asLink'
import { IconList } from '~/comps/IconList'

const Home = () => {
	const [emoji, setEmoji] = useState<string | undefined>('🚧')
	const refCopy = useRefForClipboard()
	const refCopy2 = useRefForClipboard()
	const html = `data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg"><text y="32" font-size="32">${emoji}</text></svg>`
	return (
		<div>
			<IconList emoji={emoji} setEmoji={setEmoji} />
			<div>
				<TextField
					type="text"
					value={html}
					fullWidth
					disabled={!emoji}
					inputProps={{ readOnly: true }}
				/>
				<Button data-clipboard-text={html} ref={refCopy}>
					copy
				</Button>
			</div>
			<div>
				<TextField
					type="text"
					value={asLink(html)}
					fullWidth
					disabled={!emoji}
					inputProps={{ readOnly: true }}
				/>
				<Button data-clipboard-text={asLink(html)} ref={refCopy2}>
					copy
				</Button>
			</div>
		</div>
	)
}
export default Home

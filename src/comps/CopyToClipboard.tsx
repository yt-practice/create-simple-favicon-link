import React from 'react'
import { useState, useEffect, useRef } from 'react'
import type { FC } from 'react'

import Clipboard from 'clipboard'
import Button from '@material-ui/core/Button'

export const CopyToClipboard: FC<{ text: string }> = ({ text }) => {
	const refCopy = useRef<HTMLButtonElement | null>(null)
	useEffect(() => {
		if (!refCopy.current) return
		const obj = new Clipboard(refCopy.current)
		return () => obj.destroy()
	}, [refCopy.current])
	return (
		<Button data-clipboard-text={text} ref={refCopy}>
			copy
		</Button>
	)
}

export default CopyToClipboard

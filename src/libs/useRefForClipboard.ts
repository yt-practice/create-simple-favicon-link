import { useEffect, useRef } from 'react'
import Clipboard from 'clipboard'

export const useRefForClipboard = () => {
	const refCopy = useRef<HTMLButtonElement | null>(null)
	useEffect(() => {
		if (!refCopy.current) return
		const obj = new Clipboard(refCopy.current)
		return () => obj.destroy()
	}, [refCopy.current])
	return refCopy
}

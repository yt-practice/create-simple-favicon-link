export const asLink = (href: string) =>
	`<link rel="icon" href="${escapeHtml(href)}" type="image/svg+xml" />`

const escapeHtml = (val: unknown) =>
	String(val).replace(
		/[&'`"<>]/g,
		match =>
			({
				'&': '&amp;',
				"'": '&#x27;',
				'`': '&#x60;',
				'"': '&quot;',
				'<': '&lt;',
				'>': '&gt;',
			}[match as '&' | "'" | '`' | '"' | '<' | '>'] || match),
	)

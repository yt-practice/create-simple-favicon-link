import { join } from 'path'
import {
	remove,
	readJson,
	writeJson,
	readFile,
	writeFile,
	mkdirp,
} from 'fs-extra'

const main = async () => {
	const mpath = join(__dirname, '../node_modules/@joeattardi/emoji-button')
	const json = await readJson(join(mpath, 'package.json'))
	if (!json.module && json.main) {
		json.module = json.main
		json.main = 'cjs/index.js'
		await writeJson(join(mpath, 'package.json'), json)
		const text = await readFile(join(mpath, 'dist/index.js'), 'utf-8')
		await mkdirp(join(mpath, 'cjs')).catch(Boolean)
		await writeFile(
			join(mpath, 'cjs/index.js'),
			text.replace(
				'export{rn as EmojiButton}',
				'module.exports={EmojiButton:rn}',
			),
		)
	}
	await remove(join(__dirname, '../out'))
	await remove(join(__dirname, '../.next'))
}

module.parent ||
	main().catch(x => {
		console.error(x)
		process.exit(1)
	})
